use std::fmt::{Display, Formatter};

#[derive(Clone, Default, Debug, Eq, PartialEq)]
pub struct ValidatorFinalizeGrandpaKey(String);

impl Display for ValidatorFinalizeGrandpaKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl<S: AsRef<str>> From<S> for ValidatorFinalizeGrandpaKey {
    fn from(val: S) -> Self {
        ValidatorFinalizeGrandpaKey(val.as_ref().to_string())
    }
}
