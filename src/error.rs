use crate::generation::json::error::Error as JsonError;
use std::{ffi::OsString, process::ExitStatus, string::FromUtf8Error};
use thiserror::Error;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum Error {
    #[error(transparent)]
    ArgNotConvertibleToUtf8(#[from] FromUtf8Error),
    #[error("{}: {}", .1, .0)]
    IoError(std::io::Error, String),
    #[error("{:?}: {:?}", .1, .0)]
    ShellError(ExitStatus, String),
    #[error("{:?}", .0)]
    FsExtra(String),
    #[error("Error: expected chainspec template file at {:?}, but it isn't there.", .0)]
    ChainSpecSourceFileMissing(std::path::PathBuf),
    #[error("Error: unable to losslessly convert '{:?}' into a String", .0)]
    OsStringIsNotValidString(OsString),
    #[error(transparent)]
    JsonError(#[from] JsonError),
}

impl From<fs_extra::error::Error> for Error {
    fn from(err: fs_extra::error::Error) -> Self {
        let msg = format!("{:?}", err);
        Error::FsExtra(msg)
    }
}
