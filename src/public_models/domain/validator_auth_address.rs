use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

#[derive(Clone, Default, Deserialize, Debug, Eq, PartialEq, Serialize)]
pub struct ValidatorAuthAddress(String);

impl<S: AsRef<str>> From<S> for ValidatorAuthAddress {
    fn from(val: S) -> Self {
        ValidatorAuthAddress(val.as_ref().to_string())
    }
}

impl Display for ValidatorAuthAddress {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
