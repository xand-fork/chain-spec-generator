use crate::public_models;
use std::num::NonZeroU64;

use serde_derive::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigEmissionRate {
    pub minor_units_per_validator_emission: u64,
    pub block_quota: NonZeroU64,
}

#[cfg(test)]
impl Default for ConfigEmissionRate {
    fn default() -> Self {
        ConfigEmissionRate {
            minor_units_per_validator_emission: 0,
            block_quota: 1.try_into().expect("This works!"),
        }
    }
}

impl From<&ConfigEmissionRate> for public_models::validator_emissions::EmissionRate {
    fn from(config_emission_rate: &ConfigEmissionRate) -> Self {
        public_models::validator_emissions::EmissionRate {
            minor_units_per_validator_emission: config_emission_rate
                .minor_units_per_validator_emission,
            block_quota: config_emission_rate.block_quota,
        }
    }
}
