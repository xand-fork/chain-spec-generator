use std::fmt::{Display, Formatter};

#[derive(Clone, Default, Debug, Eq, PartialEq)]
pub struct ValidatorProduceAuraKey(String);

impl Display for ValidatorProduceAuraKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl<S: AsRef<str>> From<S> for ValidatorProduceAuraKey {
    fn from(val: S) -> Self {
        ValidatorProduceAuraKey(val.as_ref().to_string())
    }
}
