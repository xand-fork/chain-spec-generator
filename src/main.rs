use chain_spec_generator::cli::{lib_main, CliOptions};
use structopt::StructOpt;

fn main() {
    let result = lib_main(CliOptions::from_args());

    match result {
        Ok(output) => println!("{}", output),
        Err(error) => {
            eprintln!("{}", error);
            std::process::exit(1);
        }
    }
}
