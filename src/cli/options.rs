use std::path::PathBuf;

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "chain-spec-generator",
    about = "A tool to generate Xand chainspecs."
)]
pub enum CliOptions {
    /// Generates a chainspec given the initial on-chain nodes.
    #[structopt(name = "generate")]
    Generate {
        #[structopt(flatten)]
        args: GenerateCommandArgs,
    },
}

#[derive(Debug, StructOpt)]
pub struct GenerateCommandArgs {
    /// Configuration file specifying the chainspec to generate.
    #[structopt(long = "config", parse(from_os_str))]
    pub config_file: PathBuf,

    /// Path to a pre-build chainspec template zip file.
    #[structopt(long = "chainspec-zip", parse(from_os_str))]
    pub chainspec_zip: PathBuf,

    /// Directory to write the chainspec into.
    #[structopt(short = "o", long = "output-dir", parse(from_os_str))]
    pub output_dir: PathBuf,
}
