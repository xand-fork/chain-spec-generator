use crate::public_models::domain::{
    address::Address, egress_ip_range::EgressIpRange, encryption_pub_key::EncryptionPubKey,
};

#[derive(Clone, Debug)]
pub struct TrustIdentity {
    pub address: Address,
    pub pub_key: EncryptionPubKey,
    pub cidr_blocks: Vec<EgressIpRange>,
}

impl TrustIdentity {
    pub fn new(
        address: Address,
        pub_key: EncryptionPubKey,
        egress_ips: Vec<EgressIpRange>,
    ) -> Self {
        Self {
            address,
            pub_key,
            cidr_blocks: egress_ips,
        }
    }
}
