use crate::error::Error;
use std::process::{Command, Output};

pub fn shell_out(cmd: &str) -> Result<Output, Error> {
    let output = Command::new("sh")
        .arg("-c")
        .arg(cmd)
        .output()
        .map_err(|e| {
            Error::IoError(
                e,
                format!(
                    "Internal error occurred while executing command \"{}\"",
                    cmd
                ),
            )
        })?;

    match &output.status.success() {
        true => Ok(output),
        false => {
            let stderr_str = String::from_utf8(output.stderr)?;

            Err(Error::ShellError(output.status, stderr_str))
        }
    }
}
