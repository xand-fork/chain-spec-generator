TAG_BASE := "gcr.io/xand-dev/chain-spec-generator"

TOML_PATH := "../Cargo.toml"
TOML_VERSION := $(shell toml get $(TOML_PATH) package.version | tr -d \")

BASE_VERSION := $(shell echo $(TOML_VERSION) | sed s/-beta.*//)

default: build

executable:
	rm -rf ./build-artifacts
	mkdir ./build-artifacts

	cd ../ && cargo build --release
	cp ../target/release/chain-spec-generator ./build-artifacts/

build: executable
	docker build . -t $(TAG_BASE):$(TOML_VERSION)

check-gcr-version-conflict:
	# Check if the release version has already been released by someone else.
	./check-gcr-version-conflict.sh $(TAG_BASE) $(BASE_VERSION)

	# Check if this specific version has already been released by someone else.
	./check-gcr-version-conflict.sh $(TAG_BASE) $(TOML_VERSION)

publish: check-gcr-version-conflict
	docker push $(TAG_BASE):$(TOML_VERSION)
