pub(crate) mod commands;
mod options;
mod output;

use commands::{run_generate_command, GenerateError};

pub use self::options::CliOptions;
pub use self::output::CliOutput;

#[derive(Debug, thiserror::Error)]
pub enum CliError {
    #[error("{}", .0)]
    Generate(#[from] GenerateError),
}

pub fn lib_main(command: CliOptions) -> Result<CliOutput, CliError> {
    match command {
        CliOptions::Generate { args } => run_generate_command(args)
            .map(Into::into)
            .map_err(Into::into),
    }
}
