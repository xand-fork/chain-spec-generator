use crate::public_models::domain::{address::Address, egress_ip_range::EgressIpRange};

#[derive(Clone, Debug)]
pub struct LimitedAgentIdentity {
    pub address: Address,
    pub cidr_blocks: Vec<EgressIpRange>,
}
