#!/usr/bin/env bash

set -e # Exit on error
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -o pipefail

function helptext {
HELPTEXT="
    This script is intended to be run by automation or humans. Given a configuration file (specifying
    identities, allowlists, block config) and a path to a chain spec .json file, and path to output file,
    merge the local configurations and write the resulting chain spec to the target output file.

    Arguments:
        IDENTITIES_CONFIG_FILE (Required) = YAML file specifying initial on-chain identities, for chain-spec-generator
        VALIDATOR_DOCKER_IMAGE_VERSION_ARG (Required) = Version of the docker image
        OUTPUT_DIR_ARG (Required) = Path to write chainspec intermediate files out to.

    Expected Environment Variables:
        ARTIFACTORY_USER (Required) = Artifactory Username to use to download the chainspec template.
        ARTIFACTORY_PASS (Required) = Artifactory Password to use to download the chainspec template.
"
echo "$HELPTEXT"
}

function error {
    echo "$1"
    exit 1
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    error "$(helptext)

Missing arguments"
fi

# Verify args are set
IDENTITIES_CONFIG_FILE_ARG=${1:?"$( error 'IDENTITIES_CONFIG_FILE must be set' )"}
VALIDATOR_DOCKER_IMAGE_VERSION_ARG=${2:?"$( error 'VALIDATOR_DOCKER_IMAGE_VERSION must be set' )"}
OUTPUT_DIR_ARG=${3:?"$( error 'OUTPUT_DIR must be set' )"}

ARG_SUMMARY="
Args:
  IDENTITIES_CONFIG_FILE_ARG         = ${IDENTITIES_CONFIG_FILE_ARG}
  VALIDATOR_DOCKER_IMAGE_VERSION_ARG = ${VALIDATOR_DOCKER_IMAGE_VERSION_ARG}
  OUTPUT_DIR_ARG                     = ${OUTPUT_DIR_ARG}
"
echo "$ARG_SUMMARY"

# Check for expected env vars
ARTIFACTORY_USER=${ARTIFACTORY_USER:?"$( error 'ARTIFACTORY_USER must be set')"}
ARTIFACTORY_PASS=${ARTIFACTORY_PASS:?"$( error 'ARTIFACTORY_PASS must be set')"}

# Constants
DOCKER_REGISTRY="gcr.io/xand-dev"

mkdir -p $OUTPUT_DIR_ARG

# Download and unzip chain spec template
ZIP_FILENAME="chain-spec-template.${VALIDATOR_DOCKER_IMAGE_VERSION_ARG}.zip"
CHAIN_SPEC_TEMPLATE_URL="https://transparentinc.jfrog.io/artifactory/artifacts-internal/chain-specs/templates/$ZIP_FILENAME"

curl --fail -L \
  -u "$ARTIFACTORY_USER:$ARTIFACTORY_PASS" \
  "$CHAIN_SPEC_TEMPLATE_URL" \
  -o $OUTPUT_DIR_ARG/$ZIP_FILENAME

# Run CLI to build the modified, human-readable chain spec
printf "\n\n---Generating chainspec---\n\n"

cargo run -- \
  generate \
    --chainspec-zip "$OUTPUT_DIR_ARG/$ZIP_FILENAME" \
    --config $IDENTITIES_CONFIG_FILE_ARG \
    --output-dir $OUTPUT_DIR_ARG

# Use validator binary to create "raw" version of chain spec
printf "\n\n---Validating chainspec---\n\n"
docker pull ${DOCKER_REGISTRY}/validator:$VALIDATOR_DOCKER_IMAGE_VERSION_ARG
docker run --rm -v $(realpath $OUTPUT_DIR_ARG/chainSpecModified.json):/xand/chainSpecModified.json --entrypoint ./xandstrate ${DOCKER_REGISTRY}/validator:$VALIDATOR_DOCKER_IMAGE_VERSION_ARG build-spec --raw --chain /xand/chainSpecModified.json > $OUTPUT_DIR_ARG/chainSpecRaw.json
