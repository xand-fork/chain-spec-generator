use serde_derive::Deserialize;

use crate::{
    cli::commands::GenerateError,
    public_models::domain::{address::Address, encryption_pub_key::EncryptionPubKey},
};

use super::parse_cidr_block_list;

#[derive(Debug, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct TrustIdentityConfiguration {
    pub address: Address,
    pub encryption_pub_key: EncryptionPubKey,

    pub ip_address_ranges: Vec<String>,
}

impl TrustIdentityConfiguration {
    pub fn build_trust_identity(&self) -> Result<crate::TrustIdentity, GenerateError> {
        Ok(crate::TrustIdentity {
            address: self.address.clone(),
            pub_key: self.encryption_pub_key.clone(),
            cidr_blocks: parse_cidr_block_list(&self.ip_address_ranges)?,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::TrustIdentityConfiguration;

    use crate::cli::GenerateError;
    use crate::public_models::domain::{address::Address, encryption_pub_key::EncryptionPubKey};

    use assert_matches::assert_matches;

    #[test]
    fn build_member_identity__can_parse_valid_ip() {
        // Given
        let config = TrustIdentityConfiguration {
            address: Address("someaddr".into()),
            encryption_pub_key: EncryptionPubKey([5; 32]),
            ip_address_ranges: vec!["127.0.0.1".into()],
        };

        // When
        let ident = config.build_trust_identity().unwrap();

        // Then
        assert_eq!(ident.address, Address("someaddr".into()));
        assert_eq!(ident.cidr_blocks, vec!["127.0.0.1".parse().unwrap()]);
        assert_eq!(ident.pub_key, EncryptionPubKey([5; 32]));
    }

    #[test]
    fn build_member_identity__errors_on_invalid_ip() {
        // Given
        let config = TrustIdentityConfiguration {
            address: Address("someaddr".into()),
            encryption_pub_key: EncryptionPubKey([5; 32]),
            ip_address_ranges: vec!["notARealIpAddress".into()],
        };

        // When
        let ident = config.build_trust_identity();

        // Then
        assert_matches!(ident.err().unwrap(), GenerateError::AddressParse(..))
    }

    #[test]
    fn from_str__can_parse_yaml_str() {
        let source_str = r#"
        address: 5ES55AwFr5uFKoPTyKpdD4G9VcaShVEg9WnWkXpPFGzMQVRV
        encryption_pub_key: E2F4aqcGWW4cKznvNiArbTErmqGjuSyAQydGDEnmy4Er
        ip_address_ranges: [ "127.0.0.1" ]
        "#;

        let config: TrustIdentityConfiguration = serde_yaml::from_str(source_str).unwrap();

        assert_eq!(
            config.address.0,
            "5ES55AwFr5uFKoPTyKpdD4G9VcaShVEg9WnWkXpPFGzMQVRV"
        );
        assert_eq!(
            config.encryption_pub_key.0,
            bs58::decode("E2F4aqcGWW4cKznvNiArbTErmqGjuSyAQydGDEnmy4Er")
                .into_vec()
                .unwrap()[..]
        );
        assert_eq!(config.ip_address_ranges[..], ["127.0.0.1"]);
    }
}
