pub(crate) mod error;

use error::{Error, Result};

pub(crate) type JsonValue = serde_json::Value;
pub(crate) type JsonMap = serde_json::map::Map<String, JsonValue>;

pub(crate) trait JsonTryGetMut {
    fn try_get_mut(&mut self, key: &str) -> Result<&mut JsonValue>;
}

pub(crate) trait JsonTryAsObjectMut {
    fn try_as_object_mut(&mut self) -> Result<&mut JsonMap>;
}

impl JsonTryGetMut for JsonValue {
    fn try_get_mut(&mut self, key: &str) -> Result<&mut JsonValue> {
        self.get_mut(key)
            .ok_or_else(|| Error::JsonKeyNotFound(key.to_string()))
    }
}

impl JsonTryAsObjectMut for JsonValue {
    fn try_as_object_mut(&mut self) -> Result<&mut JsonMap> {
        self.as_object_mut().ok_or(Error::JsonValueIsNotObject)
    }
}

impl JsonTryGetMut for JsonMap {
    fn try_get_mut(&mut self, key: &str) -> Result<&mut JsonValue> {
        self.get_mut(key)
            .ok_or_else(|| Error::JsonKeyNotFound(key.to_string()))
    }
}
