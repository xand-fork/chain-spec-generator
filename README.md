# chain-spec-generator

A tool to generate Xand chainspecs.

## Usage

You will need a YAML configuration file specifying the initial network
participants. See `sample_config.yaml` in this repo for an example.

### Download a chain spec zip from Artifactory

Download the `CHAINSPEC_VERSION` template you'd like to use into a `./chainspec_templates` directory:

```bash
CHAINSPEC_VERSION="7.11.0"
ZIP_FILENAME="chain-spec-template.${CHAINSPEC_VERSION}.zip"

wget --directory-prefix=./chainspec_templates/ --user=$ARTIFACTORY_USER  --password=$ARTIFACTORY_PASS \
    https://transparentinc.jfrog.io/artifactory/artifacts-internal/chain-specs/templates/${ZIP_FILENAME}
```

### Option 1: Building and running from source

The tool can be run locally from the repo root via Cargo:

```
cargo run generate --chainspec-zip ./chainspec_templates/$ZIP_FILENAME --config my_config.yaml --output-dir ./chainspec_output
```

### Option 2: Running via Docker

The CLI is also published as a Docker image. It can be used as follows,
replacing `0.3.1` with the latest (or desired) version:

```
docker run --rm -it \
  -v $(pwd)/my_config.yaml:/config.yaml \
  -v $(pwd)/chainspec_templates/$ZIP_FILENAME:/chainspec_template.zip \
  -v $(pwd)/chainspec_output:/output \
  gcr.io/xand-dev/chain-spec-generator:0.3.1 \
  generate \
  --chainspec-zip /chainspec_template.zip \
  --config /config.yaml \
  --output-dir /output
```

## Manually testing if validator can parse chain spec

You can test if the chain spec is correctly formatted for a given validator version
by using the validator docker image to parse and produce a "raw" version of the chain spec
```bash
docker run gcr.io/xand-dev/validator:$CHAINSPEC_VERSION
docker run --rm \
  -v $(pwd)/chainspec_output/chainSpecModified.json:/xand/chainSpecModified.json \
   --entrypoint ./xandstrate \
   gcr.io/xand-dev/validator:$CHAINSPEC_VERSION build-spec --raw --chain /xand/chainSpecModified.json > $(pwd)/chainspec_output/chainSpecRaw.json
```

## Chainspec versioning for snapshot tests

To help mitigate bugs we rely on [snapshot
tests](https://insta.rs/). Rather than letting the chainspec
generation snapshot tests potentially fail as the chainspec template
published by thermite evolves, we explicitly pin the version used. A
copy of the pinned chainspec template ZIP is located in
`./.test-assets`.

Note that the pinned version is independent of the
`CHAINSPEC_VERSION` environment variable recommended above to users
for convenience as they run the generator. However, it is recommended
to periodically bump the template version used by the snapshot tests
to detect divergences early on.
