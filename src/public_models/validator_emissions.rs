use std::num::NonZeroU64;

#[derive(Debug, Clone)]
pub struct EmissionRate {
    pub minor_units_per_validator_emission: u64,
    pub block_quota: NonZeroU64,
}
