use std::str::FromStr;

use crate::{cli::commands::GenerateError, public_models::domain::egress_ip_range::EgressIpRange};

pub fn parse_cidr_block_list(cidr_blocks: &[String]) -> Result<Vec<EgressIpRange>, GenerateError> {
    cidr_blocks
        .iter()
        .map(|ip| FromStr::from_str(ip).map_err(Into::into))
        .collect()
}

#[cfg(test)]
mod tests {
    use super::parse_cidr_block_list;

    #[test]
    fn parse_cidr_block_list__valid_ips_parse_successfully() {
        let strings: &[String] = &[
            "127.0.0.1".into(),
            "192.168.55.55".into(),
            "1.2.3.4/24".into(),
        ];

        let cidr_blocks = parse_cidr_block_list(strings).unwrap();

        assert_eq!(
            cidr_blocks
                .iter()
                .map(|b| b.to_string())
                .collect::<Vec<_>>()[..],
            ["127.0.0.1/32", "192.168.55.55/32", "1.2.3.4/24",]
        );
    }

    #[test]
    fn parse_cidr_block_list__invalid_ip_address_returns_error() {
        let strings: &[String] = &["127.0.0.1".into(), "foo".into(), "1.2.3.4".into()];

        let cidr_blocks_error = parse_cidr_block_list(strings).unwrap_err();

        assert_eq!(
            format!("{}", cidr_blocks_error),
            "failed to parse CIDR address string: invalid address: foo"
        );
    }

    #[test]
    fn parse_cidr_block_list__multiple_invalid_ip_addresses_return_first_error() {
        let strings: &[String] = &["127.0.0.1".into(), "foo".into(), "bar".into()];

        let cidr_blocks_error = parse_cidr_block_list(strings).unwrap_err();

        assert_eq!(
            format!("{}", cidr_blocks_error),
            "failed to parse CIDR address string: invalid address: foo"
        );
    }
}
