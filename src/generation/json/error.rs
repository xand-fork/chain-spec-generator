use thiserror::Error;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Error: expected JSON key {:?}, but it isn't there.", .0)]
    JsonKeyNotFound(String),
    #[error("Error: expected JSON value to be an object.")]
    JsonValueIsNotObject,
}
