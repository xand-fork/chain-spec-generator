# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [13.0.1] 2022-12-15
### Added
* Warning to use a large block_quota in the sample config

## [13.0.0] 2022-12-14
### Added
* Expand validator emissions to include block quota

## [12.0.0] 2022-10-24
### Added
* Add validator encryption keys

## [11.0.1] 2022-10-18
### Changed
* Add the confidentiality pallet back in to be compliant with the current validator version
* Remove values no longer in the xandstrate pallet

## [11.0.0] 2022-10-12
### Changed
* Remove non-confidentiality mode

## [9.0.0] 2022-08-23
### Changed
* Chainspec includes nomenclature changes

## [8.0.0] 2022-06-10
### Removed
* Confidential network chainspec no longer includes `pendingClearRedeems` 

## [7.0.0] 2022-02-22
### Added
* Confidential network chainspec now includes `pendingClearRedeems` to satisfy Validator Redemption

## [Unreleased]
<!-- When a new release is made, update tags and the URL hyperlinking the diff, see end of document -->
<!-- You may also wish to list updates that are made via MRs but not yet cut as part of a release -->



<!-- When a new release is made, add it here
e.g. ## [0.8.2] - 2020-11-09 -->
<!-- Example -->
[Unreleased]: https://gitlab.com/TransparentIncDevelopment/product/libs/xand_ledger/-/compare/0.25.0...master
